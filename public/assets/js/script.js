let toDoList = [];
let newToDo = document.getElementById("newToDo");
let addToDoButton = document.getElementById("addToDo");
let toDoListDisplay = document.getElementById("toDoList");
let clearToDoButton = document.getElementById("clearToDo");
let numberOfTodos = document.getElementById("numberOfToDos")
let firstItem = document.getElementById("firstToDo");
let lastItem = document.getElementById("lastToDo");
let toDoListJudger = document.getElementById("toDoListJudger");

function addNewTodo(newTodo) {
	if (newToDo.value === "") return;
	toDoList.push(newTodo);
	toDoListDisplay.innerHTML = "";
	toDoList.forEach(function(toDo, index) {
		let toDoItem = document.createElement("li");
		// toDoItem.className = "list-group-item";
		toDoItem.innerHTML = `${index + 1}. ${toDo}`;
		toDoListDisplay.appendChild(toDoItem);
	});
	newToDo.value = "";
	newToDo.focus();
}

function clearToDoList() {
	toDoList = [];
	toDoListDisplay.innerHTML = "";
	newToDo.focus();
}

function updateToDoJudger() {
	let listSize = toDoList.length;
	let judgeMessage;
	if (listSize > 10) {
		judgeMessage = "You are awesome! Great job!"
	} else if (listSize === 10) {
		judgeMessage = "Very good! You made it"
	} else if (listSize >= 5) {
		judgeMessage = "You are good!"
	} else {
		judgeMessage = "Please improve your work next time";
	}

	toDoListJudger.innerHTML = judgeMessage;
}

function updateToDoStatistics() {
	numberOfTodos.innerHTML = `Number of Items: ${toDoList.length}`;
	if (toDoList.length > 0) {
		firstItem.innerHTML = `First Item: ${toDoList[0]}`;
		lastItem.innerHTML = ` Last Item: ${toDoList[toDoList.length -1]}`;	
	} else {
		firstItem.innerHTML = "First Item: n/a";
		lastItem.innerHTML = "Last Item: n/a";
	}
	
}

addToDoButton.addEventListener('click', function() {
	addNewTodo(newToDo.value);
	updateToDoStatistics();
	updateToDoJudger();
});

clearToDoButton.addEventListener('click', function() {
	clearToDoList();
	updateToDoStatistics();
	updateToDoJudger();
})